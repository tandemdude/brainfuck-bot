import aiohttp


class StatsWorker:
	def __init__(self):
		self.bot_id = "651090709058682890"

	async def post_stats(self, guilds, users):
		async with aiohttp.ClientSession() as session:
			try:
				async with session.post(
					f"https://top.gg/api/bots/{self.bot_id}/stats",
					headers={"Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1MTA5MDcwOTA1ODY4Mjg5MCIsImJvdCI6dHJ1ZSwiaWF0IjoxNTc3OTExNjMxfQ.B-W9Gab_r-iVj1HDh0Ai9L3hnO8X05U3LL33gMFrBTo", "Content-Type": "application/json"},
					json={"server_count": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to top.gg")
			except Exception as e:
				print("Failed to post stats to top.gg")
				print(e)
			try:
				async with session.post(
					f"https://bots.ondiscord.xyz/bot-api/bots/{self.bot_id}/guilds",
					headers={"Authorization": "7430edc7c44df88758bbacda73f3b43c", "Content-Type": "application/json"},
					json={"guildCount": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to bots.ondiscord.xyz")
			except Exception as e:
				print("Failed to post stats to bots.ondiscord.xyz")
				print(e)
			try:
				async with session.post(
					f"https://discordbotlist.com/api/bots/{self.bot_id}/stats",
					headers={"Authorization": "Bot f8f873cd6d95566e743c92a48d49e7969785a5472f7966526ae0d0ef0a9ba36e"},
					json={"guilds": guilds, "users": users}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to discordbotlist.com")
			except Exception as e:
				print("Failed to post stats to discordbotlist.com")
				print(e)
			try:
				async with session.post(
					f"https://api.botlist.space/v1/bots/{self.bot_id}",
					headers={"Authorization": "9a18b12332b21541dc66af18734f7639575c7e7a22e60a4abb0f4088ea89f41ff6db835b4ffdef1acd256f92065b206a", "Content-Type": "application/json"},
					json={"server_count": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to botlist.space")
			except Exception as e:
				print("Failed to post stats to botlist.space")
				print(e)
			try:
				async with session.post(
					f"https://botsfordiscord.com/api/bot/{self.bot_id}",
					headers={"Authorization": "9a40933f7c08673a5b020198b9558d75d519382cc06a388c6e1d34acac9302d7e2bacbef9be8bd4cc68393f76314d4a498588905607e1c81bad1919a1a57c8d2", "Content-Type": "application/json"},
					json={"server_count": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to botsfordiscord.com")
			except Exception as e:
				print("Failed to post stats to botsfordiscord.com")
				print(e)
			try:
				async with session.post(
					f"https://discord.boats/api/bot/{self.bot_id}",
					headers={"Authorization": "FRVGdCLJLLDhWy72mCuyxGMT0mQe7Rs0KVKHHCGZ4p4Kl60xBmiTsVXXK2zZmXyewQbmAHXXxj6tAsr21vxVBvSlZByijAVAwfzHKw2LNr2SFw84MPxL84wlstqg5lJiRFrvX4IfRJIO5BQJzxI4liRterm", "Content-Type": "application/json"},
					json={"server_count": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to discord.boats")
			except Exception as e:
				print("Failed to post stats to discord.boats")
				print(e)
			try:
				async with session.post(
					f"https://divinediscordbots.com/bot/{self.bot_id}/stats",
					headers={"Authorization": "2367bd8b4e61ea3a55d9980a32b1a71e60fc8e1fc1606222b5f34c47b2d25fa6bdabcec5d41b70c3b7167659b3301d8db97abe8862a313052a79bc8a145d3c39", "Content-Type": "application/json"},
					json={"server_count": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to divinediscordbots.com")
			except Exception as e:
				print("Failed to post stats to divinediscordbots.com")
				print(e)
			try:
				async with session.post(
					f"https://discord.bots.gg/api/v1/bots/{self.bot_id}/stats",
					headers={"Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGkiOnRydWUsImlkIjoiMjE1MDYxNjM1NTc0NzkyMTkyIiwiaWF0IjoxNTc3ODk0MjU2fQ.is1sstwfVefpqA5MbrjDxaZwF6d-iZ-PzTkfkebNo-U"},
					json={"guildCount": guilds}
				) as resp:
					resp.raise_for_status()
					print("Posted stats to discord.bots.gg")
			except Exception as e:
				print("Failed to post stats to discord.bots.gg")
				print(e)
