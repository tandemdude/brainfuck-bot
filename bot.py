import custombot
import time
import logging


logging.basicConfig(level=logging.INFO)
# Sets up parser and reads the file containing the bot token

# List of all extensions to be loaded
extensions = ["ext.owner", "ext.extensionmanager", "ext.brainfuck", "ext.help", "ext.script_submission", "libneko.extras.superuser"]

# Declares the bot prefix and token, taking values from files
prefix = 'bf!'
token = "NjUxMDkwNzA5MDU4NjgyODkw.Xe7SRA.NgHdQUic6wkNkiyjOBV58cwfHGk"

# Main function creates bot, loads extensions and runs the bot
def run_bot():
    bot = custombot.CustomBot(prefix, True)
    if len(extensions) != 0:
        for ext in extensions:
            bot.load_extension(ext)
            print(f"Loaded ext {ext}")
    bot.run(token)


# Keeps the bot alive 
while True:
    run_bot()
    time.sleep(5)
