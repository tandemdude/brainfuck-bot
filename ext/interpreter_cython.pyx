import datetime
import time

class BrainfuckMemoryError(MemoryError):
    pass


class BrainfuckInterpreter:
    def __init__(self, code, input_chars=None):
        self.pointer = 0
        self.instruction_pointer = 0
        self.input_pointer = 0
        self.operation_count = 0
        self.parsed_code = []
        self.output = []
        self.tape = [0 for _ in range(30000)]
        self.bracemap = None
        self.code_running = False
        self.code = code

        if input_chars is not None:
            self.input = [ord(c) if ord(c) >= 0 and ord(c) <= 255 else 255 for c in input_chars]
        else:
            self.input = None

        self.code_timeout = 30
        self.tape_length_limit = 30000
        self.commands = [
            ">",
            "<",
            "+",
            "-",
            ",",
            ".",
            "[",
            "]"
        ]

    def parse_code(self, raw_code):
        parsed_code = []
        for character in raw_code:
            if character in self.commands:
                parsed_code.append(character)
        return parsed_code

    def validate_code(self, code):
        counter = 0
        for index, instruction in enumerate(code):
            if instruction == "[":
                counter -= 1
            elif instruction == "]":
                counter += 1

            if counter > 0:
                raise SyntaxError(f"Near `{code[index-1] if index > 0 else ''}{code[index]}{code[index+1] if index < len(code)-1 else ''}`")

    def buildbracemap(self, code):
        temp_bracestack, bracemap = [], {}
        for position, command in enumerate(code):
            if command == "[": 
                temp_bracestack.append(position)
            if command == "]":
                start = temp_bracestack.pop()
                bracemap[start] = position
                bracemap[position] = start
        return bracemap

    def run(self):
        self.parsed_code = self.parse_code(self.code)
        try:
            self.validate_code(self.code)
        except SyntaxError as e:
            print(f"Code validation error. {e.msg}")
            return {"status": "failed", "reason": "validation", "details": e.msg}

        code_length = len(self.parsed_code)
        self.bracemap = self.buildbracemap(self.parsed_code)
        self.code_running = True
        start = time.monotonic()
        start_perf = time.perf_counter()

        while self.code_running:
            if time.perf_counter() - start_perf > self.code_timeout:
                print("Execution cancelled")
                return {"status": "failed", "reason": "timeout"}

            self.operation_count += 1

            try:
                instruction = self.parsed_code[self.instruction_pointer]

                if instruction == ">":
                    self.pointer += 1
                    if self.pointer >= self.tape_length_limit:
                        self.pointer = 0
                elif instruction == "<":
                    self.pointer -= 1
                    if self.pointer < 0:
                        self.pointer = self.tape_length_limit - 1
                elif instruction == "+":
                    self.tape[self.pointer] += 1
                    if self.tape[self.pointer] > 255:
                        self.tape[self.pointer] = 0
                elif instruction == "-":
                    self.tape[self.pointer] -= 1
                    if self.tape[self.pointer] < 0:
                        self.tape[self.pointer] = 255
                elif instruction == ".":
                    self.output.append(chr(self.tape[self.pointer]))
                elif instruction == ",":
                    try:
                        self.tape[self.pointer] = self.input[self.input_pointer]
                        self.input_pointer += 1
                    except IndexError:
                        self.tape[self.pointer] = 0
                elif instruction == "[":
                    if self.tape[self.pointer] == 0:
                        self.instruction_pointer = self.bracemap[self.instruction_pointer]
                elif instruction == "]":
                    if self.tape[self.pointer] != 0:
                        self.instruction_pointer = self.bracemap[self.instruction_pointer]

                self.instruction_pointer += 1
                if self.instruction_pointer >= code_length:
                    self.code_running = False
            except BrainfuckMemoryError:
                self.code_running = False
                return {"status": "failed", "reason": "memory"}

        end = time.monotonic()
        tottime = (end - start) * 1000
        return {"status": "success" ,"output": ''.join(self.output), "opcount": self.operation_count, "time": tottime, "code": ''.join(self.parsed_code)}


# if __name__ == "__main__":
#     code = "> +++++ +++++>> +++++ +++++ +++++ +++++ +++ [< ++++ > -]<<<. + [ >>>>> +++++ +++++ [ < +++++ +++++ < +++++ +++++ >> -] < ---- < ---- [ <<< - >>> - ]<<< [>>>>>+<<<<<[>>>>>>+<<<<<<-]]>>>>>>[<<<<<<+>>>>>>-]<<[<<<<+>>>>-]> - [<<<.>>>[-]]<<<<<.+]"
#     bf = Brainfuck(code)
#     bf.run()