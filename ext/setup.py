from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("interpreter_cython.pyx")
)