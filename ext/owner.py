from discord.ext import commands, tasks
import discord


class Owner(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.update_stats.start()

    @commands.command()
    @commands.is_owner()
    async def kill(self, ctx):
        """Disconnects the bot from discord and stop the script"""
        await ctx.send("Bot logging out.")
        await self.bot.logout()

    @commands.command()
    async def stats(self, ctx):
        await ctx.send(
            embed=discord.Embed(
                title="Bot statistics", 
                description=f"Guilds - `{len(self.bot.guilds)}`\nUsers - `{len(self.bot.users)}`\nCommands Run - `{self.bot.commands_run}`\nUptime - {self.bot.uptime}",
                colour=0x39393f
            )
        )

    @commands.Cog.listener()
    async def on_ready(self):
        print("Bot is ready")
        c = self.bot.get_channel(708276482547384343)
        await c.send("Logged in successfully.")
        await self.bot.change_presence(
            activity=discord.Activity(
            type=discord.ActivityType.watching,
            name="for bf!help",
            )
        )

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        self.bot.commands_run += 1

    @tasks.loop(minutes=30)
    async def update_stats(self):
        if not self.bot.is_closed():
            await self.bot.post_stats()


def setup(bot):
    bot.add_cog(Owner(bot))
