import aiosqlite
import asyncio
import discord
from discord.ext import commands

class SubmissionError(SyntaxError):
    pass

class Submissions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db = "submissions.db"

    @staticmethod
    def parse_code(raw_code):
        parsed_code = []
        for character in raw_code:
            if character in ["<", ">", ",", ".", "[", "]", "+", "-"]:
                parsed_code.append(character)
        if len(parsed_code) <= 0:
            raise SyntaxError("No code provided.")
        return parsed_code

    @staticmethod
    def validate_code(code):
        counter = 0
        for index, instruction in enumerate(code):
            if instruction == "[":
                counter -= 1
            elif instruction == "]":
                counter += 1

            if counter > 0:
                raise SyntaxError(f"Near `{code[index-1] if index > 0 else ''}{code[index]}{code[index+1] if index < len(code)-1 else ''}`")

    async def add_script(self, owner, title, description, code):
        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT * FROM scripts WHERE title = ?", (title,)) as cursor:
                rows = await cursor.fetchall()

                if not rows:
                    await db.execute("INSERT INTO scripts(owner, title, description, code) VALUES(?, ?, ?, ?)", (owner, title, description, code))
                    await db.commit()
                else:
                    raise SubmissionError("A script with that name already exists.")

    async def show_script(self, title):
        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT owner, description, code FROM scripts WHERE title = ?", (title,)) as cursor:
                row = await cursor.fetchone()

                if not row:
                    raise SubmissionError("No script with that name was found.")
                else:
                    return row

    @commands.group(invoke_without_command=True)
    async def scripts(self, ctx):
        """
        Command group for managing submissions of scripts to the database.
        Scripts can be uploaded and viewed by anyone as well as inserted into code
        in the `bf!compile` command with brace notation \neg: `bf!compile +{script name}+.`
        """
        await ctx.send_help("scripts")

    @scripts.command()
    async def add(self, ctx):
        """
        Add a script to the database. The bot will prompt for input when required.
        """
        def check(m):
            return m.channel == ctx.channel and m.author == ctx.author and "{" not in m.content and "}" not in m.content

        try:
            msg = await ctx.send("Please enter the script name/title. It cannot include `{` or `}`")
            title_msg = await self.bot.wait_for("message", check=check, timeout=30)
            title = title_msg.content.strip()
            msg = await ctx.send("Please enter the script description")
            description_msg = await self.bot.wait_for("message", check=check, timeout=30)
            description = description_msg.content.strip()
            msg = await ctx.send("Please enter the script code")
            code_msg = await self.bot.wait_for("message", check=check, timeout=30)
            code = code_msg.content
        except asyncio.TimeoutError:
            return await msg.edit(content="**Timed Out**")

        try:
            parsed_code = self.parse_code(code)
            self.validate_code(parsed_code)
            await ctx.send("Code validation successful")
        except SyntaxError as e:
            return await ctx.send(
                embed=discord.Embed(
                    title="Code validation failed",
                    description=e.msg,
                    colour=0x39393f
                )
            )

        try:
            await self.add_script(ctx.author.id, title, description, ''.join(parsed_code))
            await ctx.send(
                f"Success! Your script has been submitted. It can be viewed by anyone with `{ctx.prefix}scripts show {title}`"
            )

            embed = discord.Embed(title="New Script Submitted", description=f"**{title}**\n{description}", colour=0x39393f)
            embed.add_field(name="Code", value=f"```brainfuck\n{''.join(parsed_code)}\n```")
            embed.set_footer(text=f"Submitted by {str(ctx.author)}")
            c = self.bot.get_channel(655885121521451079)
            whs = await c.webhooks()
            return await whs[0].send(embed=embed, username="Script Submissions", avatar_url=self.bot.user.avatar_url)

        except SubmissionError as e:
            return await ctx.send(
                embed=discord.Embed(
                    title="Submission failed",
                    description=e.msg,
                    colour=0x39393f
                )
            )

    @scripts.command()
    async def show(self, ctx, *, title=None):
        """
        View the details about a specific script from the database.
        """
        if title is None:
            return await ctx.send("No script specified")
        else:
            try:
                details = await self.show_script(title)
                return await ctx.send(
                    embed=discord.Embed(
                        title=title,
                        description=details[1],
                        colour=0x39393f
                    ).add_field(
                        name="Code",
                        value=f"```brainfuck\n{details[2]}\n```"
                    ).set_author(
                        name=f"Submitted by {str(self.bot.get_user(details[0]))}"
                    ).set_footer(text=f"bf!scripts compile {title}")
                )
            except SubmissionError as e:
                return await ctx.send(
                    embed=discord.Embed(
                        title="Submission fetch failed",
                        description=e.msg,
                        colour=0x39393f
                    )
                )

    @scripts.command()
    async def delete(self, ctx, *, title=None):
        """
        Delete a specified script from the database.
        """
        if title is None:
            return await ctx.send("No script specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT owner FROM scripts WHERE title = ?", (title,)) as cursor:
                row = await cursor.fetchone()

                if not row:
                    return await ctx.send("No script was found with that name.")
                else:
                    if ctx.author.id == row[0]:
                        await db.execute("DELETE FROM scripts WHERE title = ?", (title,))
                        await db.commit()
                        return await ctx.send("Script deleted successfully.")
                    else:
                        return await ctx.send("Permission denied, you are not the owner of this script.")

    @scripts.command()
    async def list(self, ctx):
        """
        List a random slection of up to 10 scripts from the database.
        """
        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT title FROM scripts ORDER BY RANDOM() LIMIT 10") as cursor:
                rows = await cursor.fetchall()

                emb = discord.Embed(title="Random selection of up to 10 scripts", description="`bf!scripts show [script]` to view\n```\n{}\n```".format("\n".join([row[0] for row in rows])), colour=0x39393f)
                return await ctx.send(embed=emb)

    @scripts.command()
    async def search(self, ctx, *, term=None):
        """
        Search for scripts in the database with a name matching or similar to the search term.
        """
        if term is None:
            return await ctx.send("No search term specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT title FROM scripts WHERE title LIKE ? ORDER BY length(title) LIMIT 10", (f"%{term}%",)) as cursor:
                rows = await cursor.fetchall()

                if not rows:
                    return await ctx.send("No scripts found for that search query.")
                else:
                    emb = discord.Embed(title="Scripts found for that search term", description="`bf!scripts show [script]` to view\n```\n{}\n```".format("\n".join([row[0] for row in rows])), colour=0x39393f)
                    return await ctx.send(embed=emb)

    @scripts.command(name="compile", aliases=["run", "execute", "interpret"])
    async def _compile(self, ctx, *, title=None):
        """
        Execute any script stored in the database.
        """
        if title is None:
            return await ctx.send("No script specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT code FROM scripts WHERE title = ?", (title,)) as cursor:
                row = await cursor.fetchone()

                if not row:
                    return await ctx.send("No script with that name could be found.")
                else:
                    await ctx.invoke(self.bot.get_command("compile"), code=row[0])
    
    @scripts.command()
    @commands.is_owner()
    async def admindelete(self, ctx, *, title=None):
        """
        Delete a specified script from the database.
        """
        if title is None:
            return await ctx.send("No script specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute("SELECT owner FROM scripts WHERE title = ?", (title,)) as cursor:
                row = await cursor.fetchone()

                if not row:
                    return await ctx.send("No script was found with that name.")
                else:
                    await db.execute("DELETE FROM scripts WHERE title = ?", (title,))
                    await db.commit()
                    return await ctx.send("Script deleted successfully.")


def setup(bot):
    bot.add_cog(Submissions(bot))
