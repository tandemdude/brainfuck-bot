import discord
from discord.ext import commands
from concurrent import futures
from ext.interpreter_cython import BrainfuckInterpreter
import asyncio
import re
import aiosqlite

diagram = """
**Background**
Brainfuck is an esoteric programming language created in 1993 by Urban Müller. 
The language contains only eight simple commands and an instruction pointer. 
While it is fully Turing-complete, it is not intended for practical use, but to challenge and amuse programmers.

**Diagram**
```
--------------------||---||--------
| 0 | 0 | 0 | 0 | 0 || 0 || 0 | 0 |
--------------------||---||--------
tape ---^     ^        ^
  cell --------        |
    head (pointer) ----
```

**Instructions**
`>` - Move the pointer right
`<` - Move the pointer left
`+` - Increment the current cell
`-` - Decrement the current cell
`.` - Output the value of the current cell
`,` - Replace the value of the current cell with input
`[` - Jump to the matching `]` instruction if the value of the current cell is zero
`]` - Jump to the matching `[` instruction if the value of the current cell is not zero

**Memory Layout**
The brainfuck tape is made of an infinite (in this case limited to 30,000) collection of 1 byte cells. 
Each cell represents a single, unsigned 8-bit number. 
Cells start initialized at zero.

Since the numbers are unsigned, there is no need for any complex integer implementation. 
If the upper limit of the cell is reached, it wraps back to zero. 
If zero is decremented, it must wrap back to 11111111.

**Notes**
- Using the `,` operator prompts the bot to wait for user input through discord.
- The `.` operator stores the output in a list which is sent to the channel at the end of execution.
- To prevent the bot hanging, execution time is limited to 30 seconds and will be cancelled if this limit is exceeded.
"""


class Brainfuck(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.executor = futures.ThreadPoolExecutor(max_workers=3)

        self.embeds = {
            "timeout": discord.Embed(
                title="Timeout",
                description="The code execution took too long so was cancelled",
                colour=0x39393f
            ),
            "memory": discord.Embed(
                title="Memory Error",
                description="The interpreter ran into a memory error, the maximum addressable storage locations are currently limited to `30000`.",
                colour=0x39393f
            )
        }

    @commands.command(aliases=["run", "execute", "interpret"])
    async def compile(self, ctx, *, code: str = None):
        """
        Parse and run some provided Brainfuck code.
        Scripts can be inserted into the code from the database with the notation `{script name}` if you so desire.
        """
        if code is None:
            return await ctx.send("No code supplied.")

        scripts_to_find = re.findall(r"{.*?}", code)
        scripts_to_find = [script.strip("{").strip("}") for script in scripts_to_find]
        code_substitutions = {}
        async with aiosqlite.connect("submissions.db") as db:
            for script in set(scripts_to_find):
                async with db.execute("SELECT code FROM scripts WHERE title = ?", (script,)) as cursor:
                    row = await cursor.fetchone()
                    if not row:
                        code_substitutions[script] = ""
                    else:
                        code_substitutions[script] = row[0]

        for string in code_substitutions.keys():
            code = code.replace(string, code_substitutions[string])

        chars = ",.<>+-[]"
        found = False
        for c in chars: 
            if c in code:
                found = True
        if not found:
            return await ctx.send("No code supplied.")

        def check(m):
            return m.channel == ctx.channel and m.author == ctx.author 

        user_input = None
        if "," in code:
            prompt = await ctx.send("Enter all inputs required for the code, in order")
            try:
                msg_input = await self.bot.wait_for("message", check=check, timeout=20)
                user_input = msg_input.content
            except asyncio.TimeoutError:
                return await prompt.edit(content="**Input timed out**")

        session_interpreter = BrainfuckInterpreter(code, user_input)
        future = self.executor.submit(session_interpreter.run)
        code_running = True
        async with ctx.channel.typing():
            while code_running:
                try:
                    response = future.result(timeout=0.05)
                    code_running = False
                except futures.TimeoutError:
                    code_running = True
                await asyncio.sleep(1)

            if response["status"] == "failed":
                if response["reason"] == "validation":
                    return await ctx.send(
                        embed=discord.Embed(
                            title="Syntax Error", 
                            description=response["details"],
                            colour=0x39393f
                        )
                    )
                else:
                    return await ctx.send(embed=self.embeds[response["reason"]])
            else:
                return await ctx.send(
                    embed=discord.Embed(
                        title="Execution Completed",
                        description="Process exited with code `0` in `{:.2f}` ms".format(response["time"]),
                        colour=0x39393f
                    ).add_field(
                        name="Code", value="```brainfuck\n{}\n```".format(response["code"])
                    ).add_field(
                        name="Output", value="```\n{}\n```".format(response["output"]), inline=False
                    ).set_footer(text="Total Operations: {}".format(response["opcount"]))
                )

    @commands.command()
    async def info(self, ctx):
        """
        Show some basic information about Brainfuck to help get you started.
        """
        await ctx.send(
            embed=discord.Embed(
                description=diagram, 
                colour=0x39393f
            ).set_footer(text="Bot coded by thomm.o#8637")
        )

    @commands.command()
    async def invite(self, ctx):
        """
        Provide an invite link to add the bot to your own server.
        """
        await ctx.send(
            "https://discordapp.com/api/oauth2/authorize?client_id=651090709058682890&permissions=84992&scope=bot"
        )


def setup(bot):
    bot.add_cog(Brainfuck(bot))
