from discord.ext import commands
import datetime
import json
from statspost import StatsWorker 


class CustomBot(commands.Bot):
	def __init__(self, prefix, case_insensitive):
		super().__init__(command_prefix=prefix, case_insensitive=case_insensitive)
		self.start_time = datetime.datetime.utcnow()
		with open("command_count.json") as file:
			self.commands_run = json.load(file)["commands_run"]
		self.statsworker = StatsWorker()

	@property
	def uptime(self):
		current_time = datetime.datetime.utcnow()
		s = (current_time - self.start_time).seconds
		m, s = divmod(s, 60)
		h, m = divmod(m, 60)
		d, h = divmod(h, 24)
		return f"`{d}`d `{h}`h `{m}`m `{s}`s"

	async def post_stats(self):
		await self.statsworker.post_stats(len(self.guilds), len(self.users))

	async def logout(self):
		with open("command_count.json", "w") as file:
			json.dump({"commands_run": self.commands_run}, file)

		await super().logout()
